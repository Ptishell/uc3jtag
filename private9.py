#! /usr/bin/python2

import jtagpwn
from uc3 import UC3_INSTR

if __name__ == "__main__":
    jtag = jtagpwn.JTAG()
    jtag.sc[0].instr = UC3_INSTR
    jtag.irscan(0, jtag.sc[0].instr[0x18])
    i = 1
    print("{0}: {1}".format(i, int(jtag.drscan(0, 4, i), 16)))
