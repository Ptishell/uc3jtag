#! /usr/bin/python2

import jtagpwn
from uc3 import UC3_INSTR

if __name__ == "__main__":
    jtag = jtagpwn.JTAG()
    jtag.sc[0].instr = UC3_INSTR
    jtag.irscan(0, jtag.sc[0].instr[0x1])
    print(jtag.drscan(0, 32, 0))
