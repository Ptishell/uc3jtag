#! /usr/bin/python2

import telnetlib


class JTAGInstr:

    def __init__(self, opcode, name, length):
        self.opcode = opcode
        self.name = name
        self.length = length

    def __str__(self):
        return "{0}:\t{1}\t\t{2}".format(hex(self.opcode),
                                         self.length,
                                         self.name)


class TAP:

    def __init__(self, s):
        sc = filter(lambda x: x != "", s.strip().split(" "))
        self.name = sc[1]
        self.enabled = sc[2]
        self.idcode = sc[3]
        self.expected = sc[4]
        self.irlen = int(sc[5], 16)
        self.ircap = sc[6]
        self.irmask = sc[7]
        self.instr = [JTAGInstr(i, "?", 0)
                      for i in range(2 ** self.irlen)]

    def __str__(self):
        return "TAP: {0} {1}\n".format(self.name, self.idcode) \
               + "\n".join([str(i) for i in self.instr])


class JTAG:

    def __init__(self):
        self.t = telnetlib.Telnet("127.0.0.1", 4444)
        self.t.read_until("> ")
        self.sc = [TAP(x) for x in
                   self.ocd_cmd("scan_chain").split("\n")[2:-1]]

    def __del__(self):
        self.t.close()

    def __str__(self):
        return "\n".join([str(i) for i in self.sc])

    def ocd_cmd(self, cmd):
        self.t.write(cmd + "\n")
        self.t.read_until("\n")
        return self.t.read_until("> ")[:-3]

    def irscan(self, tap, instr):
        return self.ocd_cmd("irscan {0} {1} -endstate IRPAUSE".format(self.sc[tap].name, instr.opcode))

    def drscan_pause(self, tap, length, value):
        return self.ocd_cmd("drscan {0} {1} {2} -endstate DRPAUSE".format(self.sc[tap].name, length, value))

    def drscan(self, tap, length, value):
        return self.ocd_cmd("drscan {0} {1} {2}".format(self.sc[tap].name, length, value))

    def lenscan(self, tap):
        padlen = 512
        for i in self.sc[tap].instr:
            self.irscan(tap, i)
            for j in range(padlen):
                self.drscan_pause(tap, 1, 1)
            l = 0
            while l <= 512 and int(self.drscan_pause(tap, 1, 0), 16):
                l += 1
            if (l == 1):
                i.name = "BYPASS"
            elif (l == 32):
                i.name = "IDCODE"
            i.length = l
            self.irscan(tap, i)
