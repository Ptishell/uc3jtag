OOCD= openocd
OCFG= bpocd.cfg

TELNET= telnet
OPORT= 4444

IRLEN=irlenscan.py
PRIV=private9.py

all::

irlen::
	./$(IRLEN)

priv::
	./$(PRIV)

cmd::
	$(TELNET) localhost $(OPORT)

server::
	$(OOCD) -f ./$(OCFG)
