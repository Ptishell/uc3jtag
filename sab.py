#! /usr/bin/python2

import jtagpwn
from uc3 import UC3_INSTR

if __name__ == "__main__":
    jtag = jtagpwn.JTAG()
    jtag.sc[0].instr = UC3_INSTR
    print(jtag)
